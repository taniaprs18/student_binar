package com.challange.Services;

import java.util.List;

import org.springframework.stereotype.Component;

import com.challange.Entity.RoleEntity;
import com.challange.Entity.UsersEntity;
import com.challange.exception.ResourceNotFoundException;



@Component
public interface UsersServices {
	UsersEntity signUp(UsersEntity body);
    RoleEntity post(RoleEntity body);
    void addRoleToUser(String username, String roleName) throws ResourceNotFoundException;
    UsersEntity getUserByUsername(String username) throws ResourceNotFoundException ;
    List<UsersEntity> getAllUsers();

    List<RoleEntity> getAllRole();

}
